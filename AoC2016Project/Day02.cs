﻿using System;
using System.Collections.Generic;

namespace AoC2016Project
{
    public class Day02
    {
        abstract class Keypad
        {
            protected Dictionary<Coordinate, string> layout;
            public Coordinate GetCoordinateInDirection(Coordinate coordinate, Direction direction)
            {
                Coordinate next_direction = coordinate + direction;

                if (this.layout.ContainsKey(next_direction))
                {
                    return next_direction;
                }
                return coordinate;
            }

            public string LabelFor(Coordinate coordinate) {
                return this.layout[coordinate];
            }
        }

        public struct Coordinate {
            public int x, y;

            public Coordinate(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

            public override string ToString()
            {
                return string.Format("[(x, y) = ({0}, {1})]", this.x, this.y);
            }

            public static Coordinate operator +(Coordinate coord, Direction direction)
            {
                switch (direction)
                {
                    case Direction.Up:
                        return new Coordinate(coord.x, coord.y - 1);
                    case Direction.Right:
                        return new Coordinate(coord.x + 1, coord.y);
                    case Direction.Down:
                        return new Coordinate(coord.x, coord.y + 1);
                    case Direction.Left:
                    default:
                        return new Coordinate(coord.x - 1, coord.y);
                }
            }
        }

        public enum Direction {
            Up,
            Right,
            Down,
            Left
        }

        class NumericKeypad : Keypad
        {
            public NumericKeypad()
            {
                layout = new Dictionary<Coordinate, string>();
                layout.Add(new Coordinate(x: 0, y: 0), "1");
                layout.Add(new Coordinate(x: 1, y: 0), "2");
                layout.Add(new Coordinate(x: 2, y: 0), "3");
                layout.Add(new Coordinate(x: 0, y: 1), "4");
                layout.Add(new Coordinate(x: 1, y: 1), "5");
                layout.Add(new Coordinate(x: 2, y: 1), "6");
                layout.Add(new Coordinate(x: 0, y: 2), "7");
                layout.Add(new Coordinate(x: 1, y: 2), "8");
                layout.Add(new Coordinate(x: 2, y: 2), "9");
            }
        }

        class AlphaNumericKeypad : Keypad
        {
            public AlphaNumericKeypad()
            {
                layout = new Dictionary<Coordinate, string>();
                layout.Add(new Coordinate(x: 2, y: 0), "1");
                layout.Add(new Coordinate(x: 1, y: 1), "2");
                layout.Add(new Coordinate(x: 2, y: 1), "3");
                layout.Add(new Coordinate(x: 3, y: 1), "4");
                layout.Add(new Coordinate(x: 0, y: 2), "5");
                layout.Add(new Coordinate(x: 1, y: 2), "6");
                layout.Add(new Coordinate(x: 2, y: 2), "7");
                layout.Add(new Coordinate(x: 3, y: 2), "8");
                layout.Add(new Coordinate(x: 4, y: 2), "9");
                layout.Add(new Coordinate(x: 1, y: 3), "A");
                layout.Add(new Coordinate(x: 2, y: 3), "B");
                layout.Add(new Coordinate(x: 3, y: 3), "C");
                layout.Add(new Coordinate(x: 2, y: 4), "D");
            }
        }

        static Direction ParseDirection(char character)
        {
            if (character.Equals('U')) 
            {
                return Direction.Up;
            } else if (character.Equals('L'))
            {
                return Direction.Left;
            } else if (character.Equals('D'))
            {
                return Direction.Down;
            } else {
                return Direction.Right;
            }
        }

        static string solveCode(Keypad keypad, string input, Coordinate starting_position)
        {
            string result = "";
            Coordinate position = starting_position;
            foreach (string line in input.Split("\n"))
            {
                foreach (char character in line)
                {
                    Direction direction = ParseDirection(character);
                    position = keypad.GetCoordinateInDirection(position, direction);

                }
                result += keypad.LabelFor(position);
            }
            return result;
        }

        public static string SolvePart1(string input) 
        {
            return solveCode(
                keypad: new NumericKeypad(),
                input: input,
                starting_position: new Coordinate(1, 1)
            );
        }

        public static string SolvePart2(string input)
        {
            return solveCode(
                keypad: new AlphaNumericKeypad(),
                input: input,
                starting_position: new Coordinate(0, 2)
            );
        }
    }
}
   