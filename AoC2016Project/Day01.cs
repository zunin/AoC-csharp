﻿using System;
using System.Collections.Generic;

namespace AoC2016Project
{
    public class Day01
    {
        struct Coordinate {
            public int x, y;

            public Coordinate(int x, int y) {
                this.x = x;
                this.y = y;
            }

            public override string ToString()
            {
                return string.Format("[(x, y) = ({0}, {1})]", this.x, this.y);
            }
        }
        struct InstructionSet
        {
            public RelativeDirection direction;
            public int length;

            public InstructionSet(RelativeDirection direction, int length)
            {
                this.direction = direction;
                this.length = length;
            }

            public override string ToString()
            {
                if (this.direction == RelativeDirection.Right) {
                    return string.Format("R{0}", length);
                }
                return string.Format("L{0}", length);
            }
        }

        enum RelativeDirection : int {
            Left = -1,
            Right = 1
        }

        enum CardinalDictection : int {
            North = 0,
            East = 1,
            South = 2,
            West = 3
        }

        static List<Coordinate> ParseInput(string input)
        {
            List<Coordinate> res = new List<Coordinate>();
            List<InstructionSet> instructions = new List<InstructionSet>();
            foreach (string instruction_set in input.Split(", "))
            {
                RelativeDirection relative_direction;
                if (instruction_set[0].Equals('L')) {
                    relative_direction = RelativeDirection.Left;
                } else {
                    relative_direction = RelativeDirection.Right;
                }

                int length = int.Parse(instruction_set.Substring(1));

                InstructionSet instruction = new InstructionSet(
                    direction: relative_direction,
                    length: length
                );

                instructions.Add(instruction);
            }
            CardinalDictection direction = CardinalDictection.North;
            Coordinate coordinate = new Coordinate(0, 0);
            foreach (InstructionSet instruction in instructions) {
                int next_direction_int = ((int)direction + (int)instruction.direction) % 4;
                if (next_direction_int < 0) {
                    next_direction_int += 4;
                }

                CardinalDictection new_direction = (CardinalDictection)Enum.Parse(direction.GetType(), next_direction_int.ToString());

                direction = new_direction;
                for (int index = 0; index < instruction.length; index++) {
                    switch (direction)
                    {
                        case CardinalDictection.North:
                            coordinate = new Coordinate(coordinate.x, coordinate.y + 1);
                            break;
                        case CardinalDictection.East:
                            coordinate = new Coordinate(coordinate.x + 1, coordinate.y);
                            break;
                        case CardinalDictection.South:
                            coordinate = new Coordinate(coordinate.x, coordinate.y - 1);
                            break;
                        case CardinalDictection.West:
                            coordinate = new Coordinate(coordinate.x - 1, coordinate.y);
                            break;
                        default:
                            break;
                    }
                    res.Add(coordinate);
                }
            }
            return res;       
        }

        private static int DistanceTo(Coordinate coordinate) 
        {
            return Math.Abs(coordinate.x) + Math.Abs(coordinate.y);
        }

        public static int SolvePart1(string input)
        {
            List<Coordinate> coordinates = ParseInput(input);
            Coordinate last_coordinate = coordinates[coordinates.Count - 1];

            return DistanceTo(last_coordinate);
        }

        public static int SolvePart2(string input)
        {
            List<Coordinate> coordinates = ParseInput(input);

            Dictionary<Coordinate, int> map = new Dictionary<Coordinate, int>();

            foreach (Coordinate coordinate in coordinates) {
                if (map.ContainsKey(coordinate)) 
                {
                    return DistanceTo(coordinate);
                } else 
                {
                    map.Add(coordinate, 1);
                }
            }

            Coordinate last_coordinate = coordinates[coordinates.Count - 1];
            return DistanceTo(last_coordinate);
        }
    }
}
