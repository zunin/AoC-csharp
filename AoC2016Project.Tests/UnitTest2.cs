﻿using System;
using Xunit;
using AoC2016Project;

namespace AoC2016Project.Tests
{
    public class UnitTestsDay02
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal("1985", Day02.SolvePart1("ULL\nRRDDD\nLURDL\nUUUUD"));
        }

        [Fact]
        public void Test2()
        {
            Assert.Equal("5DB3", Day02.SolvePart2("ULL\nRRDDD\nLURDL\nUUUUD"));
        }

        [Fact]
        public void TestCoordinateUp()
        {
            Day02.Coordinate coordinate = new Day02.Coordinate(0, 0);
            Day02.Direction up = Day02.Direction.Up;

            Assert.Equal(-1, (coordinate + up).y);
        }

        [Fact]
        public void TestCoordinateDown()
        {
            Day02.Coordinate coordinate = new Day02.Coordinate(0, 0);
            Day02.Direction down = Day02.Direction.Down;

            Assert.Equal(1, (coordinate + down).y);
        }

        [Fact]
        public void TestCoordinateRight()
        {
            Day02.Coordinate coordinate = new Day02.Coordinate(0, 0);
            Day02.Direction right = Day02.Direction.Right;

            Assert.Equal(1, (coordinate + right).x);
        }

        [Fact]
        public void TestCoordinateLeft()
        {
            Day02.Coordinate coordinate = new Day02.Coordinate(0, 0);
            Day02.Direction left = Day02.Direction.Left;

            Assert.Equal(-1, (coordinate + left).x);
        }

    }
}
