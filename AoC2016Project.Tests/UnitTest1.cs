using System;
using Xunit;
using AoC2016Project;

namespace AoC2016Project.Tests
{
    public class UnitTestsDay01
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(Day01.SolvePart1("R2, L3"), 5);
        }

        [Fact]
        public void Test2()
        {
            Assert.Equal(Day01.SolvePart1("R2, R2, R2"), 2);
        }

        [Fact]
        public void Test3()
        {
            Assert.Equal(Day01.SolvePart1("R5, L5, R5, R3"), 12);
        }

        [Fact]
        public void Test4()
        {
            Assert.Equal(Day01.SolvePart2("R8, R4, R4, R8"), 4);
        }
    }
}
